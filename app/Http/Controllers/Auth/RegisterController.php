<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Perpustakaan\Mahasiswa;
use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
use App\Http\Requests\Auth\RegisterRequest;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        // request()->validate([
        //     'name' => ['string', 'required'],
        //     'username' => ['alpha_num', 'required', 'min:3', 'max:25', 'unique:users,username'],
        //     'email' => ['email', 'required', 'unique:users,email'],
        //     'password' => ['required', 'min:6']
        // ]); 
        // dipindah ke register request rule validate nya

        $mahasiswaData = [
            'nama' => request('nama'),
            'fakultas' => request('fakultas'),
            'jurusan' => request('jurusan'),
            'handphone' => request('handphone'),
            'whatsapp' => request('whatsapp'),
            'nim' =>  $this->generateNim(),
        ];
        
        $mahasiswa = new Mahasiswa($mahasiswaData);
        $mahasiswa->save();

        $data = [
            'username' => request('username'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ];

        $user = new User($data);
        $mahasiswa->user()->save($user);
        
        return response('Thanks, you are registered.');
    }

    private function generateNim() {
        // $number = mt_rand(1000000000, 9999999999); // better than rand()
        $number = uniqid(); // alternative random. unique ID based on the microtime
        // call the same function if the barcode exists already
        if ($this->nimExists($number)) {
            return $this->generateNim();
        }
    
        // otherwise, it's valid and can be used
        return $number;
    }

    private function nimExists($number) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return Mahasiswa::whereNim($number)->exists();
    }
}
