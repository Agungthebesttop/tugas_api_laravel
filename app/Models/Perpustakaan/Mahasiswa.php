<?php

namespace App\models\Perpustakaan;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Mahasiswa extends Model
{
    protected $fillable = ['nama', 'nim', 'jurusan', 'fakultas', 'handphone', 'whatsapp'];

    public function user(){
        return $this->hasOne(User::class, 'id');
    }

    public function pinjam()
    {
        return $this->hasMany(Pinjam::class);
    }
}
